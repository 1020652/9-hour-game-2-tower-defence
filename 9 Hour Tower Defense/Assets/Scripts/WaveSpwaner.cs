using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpwaner : MonoBehaviour
{
    public Transform enemy;
    public Transform spawnPoint;
    public float countdownTimer;

    private float countdown = 2f;
    private int waveIndex = 0;
    [SerializeField] private float timeBetweenSpawn;

    void Update()
    {
        if(countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = countdownTimer;
        }

        countdown -= Time.deltaTime;

    IEnumerator SpawnWave()
        {
            waveIndex++;

            for (int i = 0; i < waveIndex; i++)
            {
                SpawnEnemy();
                yield return new WaitForSeconds(timeBetweenSpawn);
            }
        }

        void SpawnEnemy()
        {
            Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
