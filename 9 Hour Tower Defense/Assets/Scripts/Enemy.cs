using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;

    private Transform target;
    private int waypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        target = Waypoints.waypoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = target.position - transform.position;
        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            GetNextWaypoint();
        }

    void GetNextWaypoint()
        {
            if(waypointIndex >= Waypoints.waypoints.Length - 1)
            {
                Destroy(gameObject);
                return;
            }

            waypointIndex++;
            target = Waypoints.waypoints[waypointIndex];
        }

    }
}
